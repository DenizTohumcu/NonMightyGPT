# NonMightyGPT - Your Slightly Less Almighty Research Assistant

## Project Overview
Prepare to embark on a quest with NonMightyGPT, where your mission, should you choose to accept it, is to dive deep into the abyss of scientific PDFs without drowning in data. This slightly less almighty chatbot helps you, the esteemed scientific researcher, retrieve golden nuggets of information with a mere flick of a query.

## Background
NonMightyGPT emerged from the shadows of towering stacks of unread PDFs and unanswered questions. It's designed to bring light to the obscure and clarity to the confounded, leveraging the mystical arts of NLP and vector databases to deliver insights faster than you can say "Eureka!"

## Requirements

### Must Have
- A magic wand (also known as an algorithm) to parse and index PDFs into a mystical vector database.
- The ability to understand the ancient language of Academia through advanced NLP techniques.
- Fort Knox-like security measures to guard the sanctity of your data.
- An ever-growing library, because just like a wizard’s beard, a database should never stop growing.

### Should Have
- A crystal ball feature (a.k.a. recommendation system) to foresee and suggest related documents.
- The capability to play nicely with other academic toolkits and databases.

### Could Have
- A robe and wizard hat for the UI — academic style meets magical usability.

### Won't Have (initial version)
- Telepathic collaboration tools — mind reading to be developed in version 2.0.

## System Architecture
- **UI**: The magical interface where researchers cast their queries.
- **Application Server**: The brain of the operation, processing spells and managing the flow of mystical energies.
- **Vector Database**: The grand library where all knowledge is encoded into magical vectors.
- **Document Ingestion Service**: The diligent librarian, tirelessly cataloging new tomes and scrolls.

## Code Conventions
### Python with Doxygen
- Document your sorcery with Doxygen-style enchantments.
- Adhere to the sacred PEP 8 text for Pythonic rituals.
- Example of a Doxygen incantation:
  ```python
  def cast_spell(param1, param2):
      """
      @brief Conjures a minor spell of documentation.
      @param param1 The essence of the first element.
      @param param2 The essence of the second element.
      @return True if the cosmos align; otherwise, False.
      """
      return True
  ```
## Installation and Setup
Follow these arcane stepstp conjure NonMightyGPT into existence on your local apparatus.

## Implementation Steps
1. Consecrate Development Environment.
2. Invoke Document Ingestion Service.
3. Erect Vector Database.
4. Summon Application Server.
5. Bind NLP Model.
6. Weave User Interface.
7. Purify with Testing and Quality Assurance.
8. Manifest Deployment.
9. Enlighten with Training and Documentation.

## Milestones
1. Project Kickoff - The summoning circle is drawn.
2. First Prototype - The first spark of intelligence flickers.
3. Alpha Release - The entity speaks!
4. Beta Release - The entity learns.
5. Production Launch - The world rejoices.
6. Post-Launch Review - The prophecy is fulfilled.

## Contributing
Wizards and apprentices, heed this call! Your spells and incantations are welcome. Help us refine the arts of code and knowledge retrieval.

## License
This tome is bound by the MIT License, enabling free distribution of knowledge far and wide.
